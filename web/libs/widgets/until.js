define(['layer'],function(layer){

    // 分享方法


    var share = function(){
        window._bd_share_config = {
            "common"  : {
                "bdSnsKey"  : {},
                "bdText"    : "",
                "bdMini"    : "2",
                "bdMiniList": false,
                "bdPic"     : "",
                "bdStyle"   : "0",
                "bdSize"    : "16"
            }, "slide": {"type": "slide", "bdImg": "1", "bdPos": "left", "bdTop": "100"}
        };
        with (document)0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() / 36e5)];

    };


    //判断是否是IE浏览器，包括Edge浏览器兼容ie10以上

    var  IEVersion = function(){

        window.AESKey = '';
        (function() {
            //判断是否是IE浏览器，包括Edge浏览器
            function IEVersion() {
                //取得浏览器的userAgent字符串
                var userAgent = navigator.userAgent;
                //判断是否IE浏览器
                var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1;
                if (isIE) {
                    var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
                    reIE.test(userAgent);
                    var fIEVersion = parseFloat(RegExp["$1"]);
                    if (fIEVersion < 10 || !isSupportPlaceholder()) {
                        return true;
                    }
                } else {
                    return false;
                }
            }

            window.onload = function() {
                if (IEVersion()) {
                    alert('为了给您提供更优质的网页浏览体验，建议升级或更换其他浏览器');
                }
            };
        })();


    }








    //只允许输入数字
    var clearstr2 = function (inputobj) {
        inputobj.value = inputobj.value.replace(/[^0123456789]/g, '');
    };


    // 验证码
    var regex_check = function (str, reg) {
        var regexEnum = {
            mobile: "^1[3456789]\\d{9}$",//手机
            letter: "^[A-Za-z]+$",//字母
            letter_u: "^[A-Z]+$",//大写字母
            letter_l: "^[a-z]+$",//小写字母
            email: "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$", //邮件
            date: "^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$",	//日期
            idcard: "^(\\d{15}$|^\\d{18}$|^\\d{17}(\\d|X|x))$"	//身份证
        };
        var strings = str.replace(/(^\s*)|(\s*$)/g, ""), strings = strings.replace(/(\s*$)/g, "");
        var r = eval("regexEnum." + reg);
        var result = (new RegExp(r)).test(strings);
        return result;
    };

// 表单ie兼容部分
    $('[placeholder]').placeholder();
//判断是否PC或移动端true:PC false Mobile
    var browserRedirect = function () {
        var sUserAgent = navigator.userAgent.toLowerCase();
        var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
        var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
        var bIsMidp = sUserAgent.match(/midp/i) == "midp";
        var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
        var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
        var bIsAndroid = sUserAgent.match(/android/i) == "android";
        var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
        var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
        if (!(bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM)) {
            return true;
        } else {
            return false;
        }
    };
    var GetDomainName = function () {//获取域名后缀
        var url = window.location.host;
        url = url.substring(url.lastIndexOf('.'));
        if (url == '.cn') {
            url = '.com'
        }
        return url;
    };


    var GetUrlParams = function (name) {//获取url地区参数：utm_area = beijing
        var params = window.location.href;
        strparams = params.substring(params.lastIndexOf('?'));
        var arrparams = strparams.split("&");//分割
        //遍历匹配
        for ( var i = 0; i < arrparams.length; i++) {
            var arr = arrparams[i].split("=");
                if (arr[0] == name){
                return decodeURI(arr[1]);
                }
        }
        return "";
    };
    function getQueryString(name) {
   var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
   var r = window.location.search.substr(1).match(reg);
   if (r != null) return unescape(r[2]); return null;
 }
// 浏览器跳转
    var jump = function(url){
        if(!browserRedirect()){
            window.location.replace(document.location.protocol+'//'+window.location.host+'/'+url);
        }
    };
    // 获取电话号码

    var getPhoneAjax = function(sp_id,callback){
        $.ajax({
            url: document.location.protocol+'//www.xmy' + GetDomainName() + '/index.php?do=ajax&view=common&op=zt_contact&sp_id='+sp_id+'&utm_area='+ getQueryString("utm_area"),
            type: 'get',
            dataType: 'jsonp',
            jsonp: 'callback',
            success: function (data) {
                if (data.status == 0) {
                    callback(data);
                }
            }
        });
    };

    // 提交数据
    var serverAjax = function(paremt,callback){
        layer.load(1, {
            shade: [0.1, '#fff'] //0.1透明度的白色背景
        });
        $.ajax({
            url:document.location.protocol+'//www.xmy'+ GetDomainName() + '/index.php?do=ajax&view=advertis',
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            data: paremt,
            success: function (json) {
                layer.closeAll('loading');
                if (json.status !== 1) {
                    layer.alert(json.msg, {icon: 2});
                }
                callback(json);
            }
        })

    };

    $.fn.serializeObj = function(){
        var obj = {};
        $.each(this.serializeArray(),function(_index,item){
            obj[item.name] = item.value;
        });
        return obj;
    };


// 校验规则


jQuery.validator.addMethod("tel", function(value, element) {
    var tel = /^1[34578]{1}\d{9}$/;
    return this.optional(element) || (tel.test(value));
}, "电话号码格式不对");




$._config = {
    onfocusout:false,
    onkeyup:false,
    onclick:false,
    errorPlacement: function(error, element){
        if(error[0].textContent!=''){
            layer.tips(error[0].textContent,'.error',{
                tips:3
            });
        }
    },
    rules : {
        mobile : {
            required : true,
            tel:true
        },
        username:{
            required:true
        }
    },
    messages : {
        mobile : {
            required : '请输入您的手机号码',
            tel:'输入正确的手机号码'
        },
        username:{
            required:'请输入您的姓名'
        }
    }
};

   const tip = function(content,time){
        if(!content) return false;
        if(time==undefined || time==null) time=2;
        layer.alert(content);
    };


 var time;
    var countDown = function(_this,wait,num){
        clearTimeout(time);
        if (wait == 0) {
            $(_this).removeAttr("disabled");
            $(_this).text('重获验证码');
            wait=num;
            return false;
        } else {
            $(_this).attr("disabled", true);
            $(_this).text(wait + "s重新获取");
            wait--;
        }
        time=setTimeout(function () {
            countDown(_this,wait,num);
        }, 1000);
    }



  var sendMessages = function(parmer,btn,time){
        $.ajax({
            type: 'POST',
            url: document.location.protocol+'//ajax.xmy' + GetDomainName() + '/index.php?do=ajax&view=send_code&auth=register&type=mobile&ignore_code=1&condit=mobile&auth_mobile=1',
            dataType: 'jsonp',
            data:parmer,
            jsonp: 'callback',
            success: function (data) {
                if(data.status==1){
                    countDown(btn,time,time);
                }else{
                    var a=data.msg;
                    tip(a);
                }
            },
            error: function (data) {
                var a=data.msg;
                tip(a);
                // tip("服务器繁忙，请稍后再试！");
            }
        });
    }





    return {
        jump : jump,
        getPhoneAjax : getPhoneAjax,
        serverAjax:serverAjax,
        regex_check:regex_check,
        share:share,
        GetDomainName:GetDomainName,
        clearstr2:clearstr2,
        GetUrlParams: GetUrlParams,
        sendMessages:sendMessages 
    };

});
