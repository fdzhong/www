
require.config({
    waitSeconds: 0,
    // urlArgs:'v='+(new Date()).getTime(),
    paths: {
        'normalize':'../libs/require/css/normalize',
        'css-builder':'../libs/require/css/css-builder',
        "css":"../libs/require/css/css.min",
        "jquery": "../libs/widgets/jquery/jquery-1.11.2",
        "layer":"../libs/widgets/layer/layer",
        "swiper":"../libs/widgets/swiper/swiper.min",
        "swiper_3_4_2":"../libs/widgets/swiper/swiper-3.4.2.min",
        "avalon_co":'../libs/widgets/avalon/avalon',
        "avalon":'../libs/widgets/avalon/avalon.config',
        "vue":'../libs/widgets/vue/vue',
        "until":'../libs/widgets/until',
        "convert":'../libs/widgets/convert/convert',
        "validate":'../libs/widgets/validate/jquery.validate.min',
        "placeholder":'../libs/widgets/common/jquery.placeholder',
        "superSlide":'../libs/widgets/superSlide/jquery.SuperSlide.2.1.1',
        "effect":'../libs/widgets/common/jquery.animation.effect',
        "jcarousel":'../libs/widgets/jcarousel/jquery.jcarousel',
        "imageflow":'../libs/widgets/imageflow/imageflow'
    },
    shim: {
        "jquery": {
           "deps":[]
        },
        "layer":{
            "deps":['jquery','css!../libs/widgets/layer/theme/default/layer.css']
        },
        "swiper":{
            "deps":['css!../libs/widgets/swiper/swiper.min.css']
        },
        "swiper_3_4_2":{
            "deps":['css!../libs/widgets/swiper/swiper-3.4.2.min.css']
        },
        "avalon_co":{
            "deps":[]
        },
        "avalon":{
            "deps":[]
        },
        "vue":{
            "deps":[]
        },
        "until":{
            "deps":['jquery','convert','validate','placeholder']
        },
        "convert":{
            "deps":[]
        },
        "validate":{
            "deps":['jquery']
        },
        "placeholder":{
            "deps":[]
        },
        "effect":{
            "deps":[]
        },
        "superSlide":{
            "deps":['jquery']
        },
        "jcarousel":{
            "deps":['jquery']
        },
        "imageflow":{
            "deps":['jquery','css!../libs/widgets/imageflow/imageflow.css']
        }
    }
});



