
require.config({
    waitSeconds: 0,
    // urlArgs:'v='+(new Date()).getTime(),
    paths: {
        'normalize':'libs/require/css/normalize',
        'css-builder':'libs/require/css/css-builder',
        "css":"libs/require/css/css.min",
        "Zepto": "libs/widgets/zepto/zepto",
        "layer":"libs/widgets/layer/layer",
        "swiper":"libs/widgets/swiper/swiper",
        "avalon_co":'libs/widgets/avalon/avalon',
        "avalon":'libs/widgets/avalon/avalon.config',
        "vue":'libs/widgets/vue/vue',
        "until":'libs/widgets/until',
        "flexible":'libs/widgets/flexible/flexible',
        "convert":'libs/widgets/convert/convert',
        "validate":'libs/widgets/validate/zepto-mvalidate',
        "jquery":'libs/widgets/jquery/jquery-1.11.2',
        "SuperSlide":'libs/widgets/SuperSlide/SuperSlide',
        "imageflow":'libs/widgets/imageflow/imageflow',
    },
    shim: {
        "Zepto": {
           "deps":[]
        },
        "layer":{
            "deps":['Zepto','css!libs/widgets/layer/layer.css']
        },
        "swiper":{
            "deps":['css!libs/widgets/swiper/swiper.css']
        },
        "avalon_co":{
            "deps":[]
        },
        "avalon":{
            "deps":['avalon_co']
        },
        "vue":{
            "deps":[]
        },
        "until":{
            "deps":['Zepto','flexible','convert','validate']
        },
        "flexible":{
            "deps":[]
        },
        "convert":{
            "deps":[]
        },
        "validate":{
            "deps":['css!libs/widgets/validate/validate']
        },
        "jquery":{
            "deps":[]
        },
        "SuperSlide":{
            "deps":["jquery"]
        },
        "imageflow":{
            "deps":['Zepto','css!libs/widgets/imageflow/imageflow.css']
        },
        
    }
});



