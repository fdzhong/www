define(['layer','convert','avalon'], function() {

    
    // 验证码
    var regex_check = function (str, reg) {
        var regexEnum = {
            mobile: "^1[3456789]\\d{9}$",//手机
            letter: "^[A-Za-z]+$",//字母
            letter_u: "^[A-Z]+$",//大写字母
            letter_l: "^[a-z]+$",//小写字母
            email: "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$", //邮件
            date: "^\\d{4}(\\-|\\/|\.)\\d{1,2}\\1\\d{1,2}$",	//日期
            idcard: "^(\\d{15}$|^\\d{18}$|^\\d{17}(\\d|X|x))$"	//身份证
        };
        var strings = str.replace(/(^\s*)|(\s*$)/g, ""), strings = strings.replace(/(\s*$)/g, "");
        var r = eval("regexEnum." + reg);
        var result = (new RegExp(r)).test(strings);
        return result;
    };

    // 表单验证

    $.mvalidateExtend({
        user:{
            required : true,
            descriptions:{
                required:'姓名不能为空'
            }
        },
        phone:{
            required:true,
            pattern:/^1[3456789]{1}\d{9}$/,
            descriptions:{
                required:'手机号码不能为空',
                pattern:'手机号码格式有误'
            }
        },
        pCode:{
            required:true,
            pattern:/^\d+$/,
            descriptions:{
                required:'请输入6位验证码',
                pattern:'验证码格式有误'
            }
        }
    });
    //判断是否PC或移动端true:PC false Mobile
    var browserRedirect = function () {
        var sUserAgent = navigator.userAgent.toLowerCase();
        var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
        var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
        var bIsMidp = sUserAgent.match(/midp/i) == "midp";
        var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
        var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
        var bIsAndroid = sUserAgent.match(/android/i) == "android";
        var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
        var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
        if (!(bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM)) {
            return true;
        } else {
            return false;
        }
    };
    // 浏览器跳转
    var jump = function(url){
        if(browserRedirect()){
            window.location.replace(document.location.protocol+'//www.xmy'+GetDomainName()+'/'+url);
        }
    };

    $.fn.serializeObj = function(){
        var obj = {};
        $.each(this.serializeArray(),function(_index,item){
            obj[item.name] = item.value;
        });
        return obj;
    };
    var GetDomainName = function () {//获取域名后缀
        var url = window.location.host;
        url = url.substring(url.lastIndexOf('.'));
        if (url == '.cn') {
            url = '.com'
        }
        return url;
    };

    var GetUrlParams = function (name) {//获取url地区参数：utm_area = beijing
        var params = window.location.href;
        strparams = params.substring(params.lastIndexOf('?'));
        var arrparams = strparams.split("&");//分割
        //遍历匹配
        for ( var i = 0; i < arrparams.length; i++) {
            var arr = arrparams[i].split("=");
                if (arr[0] == name){
                return decodeURI(arr[1]);
                }
        }
        return "";
    };
        //判断是否PC或移动端true:PC false Mobile
    $.fn.browserRedirect=function () {
        var sUserAgent = navigator.userAgent.toLowerCase();
        var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
        var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
        var bIsMidp = sUserAgent.match(/midp/i) == "midp";
        var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
        var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
        var bIsAndroid = sUserAgent.match(/android/i) == "android";
        var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
        var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
        if (!(bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM)) {
            return true;
        } else {
            return false;
        }
    }

    const tip = function(content,time){
        if(!content) return false;
        if(time==undefined || time==null) time=2;
        layer.open({
            skin: 'msg',
            time: time,
            content:content
        })
    };

    // 异步的方法---------------------------------------------

    function GetDomainName(){//获取域名后缀
        var url = window.location.host;
        url = url.substring(url.lastIndexOf('.'));
        return url;
    }
    

    //只允许输入数字
    var clearstr2 = function (inputobj) {
        inputobj.value = inputobj.value.replace(/[^0123456789]/g, '');
    };

    var ajaxServer = function(parmer,callback){
        $.ajax({
            url:document.location.protocol+'//www.xmy' + GetDomainName() + '/index.php?do=ajax&view=advertis',
            type:'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            data:parmer,
            success: function(data) {
                callback(data);
                // 头条统计代码
                if(App.convert_id) _taq.push({"convert_id": App.convert_id, "event_type": "form"});
            },
            error: function(data) {
                tip("网络繁忙,请稍后重试");
            }
        })

    };
    //获取验证
    var sendMessages = function(parmer,btn,time){
        $.ajax({
            type: 'POST',
            url: document.location.protocol+'//ajax.xmy' + GetDomainName() + '/index.php?do=ajax&view=send_code&auth=register&type=mobile&ignore_code=1&condit=mobile&auth_mobile=1',
            dataType: 'jsonp',
            data:parmer,
            jsonp: 'callback',
            success: function (data) {
                if(data.status==1){
                    countDown(btn,time,time);
                }else{
                    var a=data.msg;
                    tip(a);
                }
            },
            error: function (data) {
                var a=data.msg;
                tip(a);
                // tip("服务器繁忙，请稍后再试！");
            }
        });
    }

    //倒计时
    /**
     *_this  是倒计时按钮
     * wait  是要倒计时时间，用来加减
     * num   倒计时时间,固定参数
     **/
    var time;
    var countDown = function(_this,wait,num){
        clearTimeout(time);
        if (wait == 0) {
            $(_this).removeAttr("disabled");
            $(_this).text('重获验证码');
            wait=num;
            return false;
        } else {
            $(_this).attr("disabled", true);
            $(_this).text(wait + "s重新获取");
            wait--;
        }
        time=setTimeout(function () {
            countDown(_this,wait,num);
        }, 1000);
    }

    // 获取电话号码

    var getTelAjax = function(sp_id,callback){
        var _this = this;
        $.ajax({
            url: document.location.protocol+'//www.xmy'+GetDomainName()+'/index.php?do=ajax&view=common&op=zt_contact&utm_area='+ GetUrlParams("utm_area")+'&sp_id='+sp_id,
            type: 'GET',
            dataType: 'jsonp',
            jsonp: 'callback',
            success: function (json) {
                if (json.status == 0) {
                    callback(json.data);
                }
            }
        })

    }

    return {
        tip:tip,
        sendMessages:sendMessages,
        ajaxServer:ajaxServer,
        getTelAjax:getTelAjax,
        countDown:countDown,
        regex_check:regex_check,
        jump:jump,
        clearstr2:clearstr2,
        GetUrlParams: GetUrlParams
    }

});
